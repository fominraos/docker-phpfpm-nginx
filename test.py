import testinfra

def check_os_type(host):
    assert host.file("/etc/os-release").contains("Alpine Linux")

def test_nginx_is_installed(host):
    nginx = host.package("nginx")
    assert nginx.is_installed

def test_nginx_version(host):
    nginx = host.package("nginx")
    assert nginx.version.startswith("1.16")